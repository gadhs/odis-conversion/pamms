'use strict'

const fsp = require('node:fs/promises')
const ospath = require('node:path')
const { posix: path } = ospath

module.exports.register = function () {
  this.once('navigationBuilt', async ({ playbook, contentCatalog }) => {
    const assemblerConfigFile = ospath.join(playbook.dir, 'antora-assembler.yml')
    const assemblerConfig = this.require('js-yaml').load(await fsp.readFile(assemblerConfigFile), 'utf-8')
    const componentsWithPdfs = assemblerConfig.component_versions.reduce((accum, it) => {
      if (it === '*') {
        contentCatalog.getComponents().forEach(({ name }) => accum.add(name))
      } else {
        it.startsWith('!') ? accum.delete(it.slice(1)) : accum.add(it)
      }
      return accum
    }, new Set())
    contentCatalog.getComponents().forEach((component) => {
      if (!componentsWithPdfs.has(component.name)) return
      component.versions.forEach((componentVersion) => {
        const pagesByUrl = componentVersion.files.reduce((accum, it) => {
          if (it.src.family === 'page' && it.out) accum.set(it.pub.url, it)
          return accum
        }, new Map())
        componentVersion.navigation.reduce((accum, entry) => {
          if (entry.content) {
            accum.push(entry)
          } else {
            entry.items.forEach((bareEntry) => accum.push(bareEntry))
          }
          return accum
        }, []).forEach((pdfRoot) => {
          const stem = toPdfStem(pdfRoot.content)
          const pdf = contentCatalog.addFile({
            asciidoc: { attributes: { docfile: `${componentVersion.version}@${component.name}::pdf$${stem}.adoc` } },
            path: `modules/ROOT/attachments/${stem}.pdf`,
            src: { component: component.name, version: componentVersion.version, module: 'ROOT', family: 'attachment', relative: `${stem}.pdf` },
          })
          if (pdfRoot.urlType === 'internal') pagesByUrl.get(pdfRoot.url).assembler = { formats: { pdf } }
          attachPdfToPages(pdf, pdfRoot.items, pagesByUrl)
        })
      })
    })
  })

  this.prependListener('beforePublish', ({ playbook, contentCatalog }) => {
    const siteUrl = playbook.site.url
    if (!siteUrl) return
    const otherPublishableResourcesByUrl = contentCatalog
      .getFiles()
      .reduce((accum, it) => it.out && it.src.family !== 'page' ? accum.set(it.pub.url, it) : accum, new Map())
    contentCatalog.getComponents().forEach((component) => {
      component.versions.forEach(({ navigation }) => {
        externalizeNonPageLinks(navigation, siteUrl, otherPublishableResourcesByUrl)
      })
    })
  })

  this.on('beforePublish', ({ contentCatalog, siteCatalog }) => {
    const pdfFiles = contentCatalog
      .findBy({ module: 'ROOT', family: 'attachment', extname: '.pdf' })
      .reduce((accum, it) => 'asciidoc' in it ? accum.set(it.asciidoc.attributes.docfile, it) : accum, new Map())
    siteCatalog.getFiles().forEach((file) => {
      if (!(file.mediaType === 'application/pdf' && file.src?.extname === '.adoc')) return
      siteCatalog.removeFile(file)
      const toFile = pdfFiles.get(`${file.src.version}@${file.src.component}::pdf$${file.src.basename}`)
      if (!toFile) return
      toFile.asciidoc = file.asciidoc
      toFile.contents = file.contents
    })
  })
}

function externalizeNonPageLinks (items, siteUrl, resourcesByUrl) {
  if (!(items && items.length)) return
  items.forEach((it) => {
    if (it.urlType === 'internal' && resourcesByUrl.has(it.url)) {
      Object.assign(it, { urlType: 'external', url: `${siteUrl}${it.url}` })
    }
    externalizeNonPageLinks(it.items, siteUrl, resourcesByUrl)
  })
}

function attachPdfToPages (pdf, items, pagesByUrl) {
  if (!(items && items.length)) return
  items.forEach((it) => {
    if (it.urlType === 'internal' && pagesByUrl.has(it.url)) {
      const page = pagesByUrl.get(it.url)
      const { module: module_, relative, stem } = page.src
      const dirname = path.dirname(relative)
      const docnameForId = dirname == '.' ? stem : `${dirname.replace(/[/]/g, '::')}::${stem}`
      const fragment = `#${module_ === 'ROOT' ? '' : module_ + ':'}${docnameForId}:::`
      page.assembler = { formats: { pdf }, fragment }
    }
    attachPdfToPages(pdf, it.items, pagesByUrl)
  })
}

function toPdfStem (title) {
  return title.toLowerCase().replace(/[()/]/g, '').replace(/[ -]+/g, '-')
}
