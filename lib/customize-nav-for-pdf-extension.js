'use strict'

/**
 * This extension looks for the ext.assembler.nav key defined in the component version descriptor. If defined, the
 * extension builds the navigation from that key and updates the navigation property on the component version with that
 * value while Assembler runs. The extension then restores the original value after all other extensions on the
 * navigationBuilt event are run (including Assembler). Note that the original navigation is ignored if this key is set.
 * In order to get the navigation builder to see the alternate nav files, this extension proxies the content catalog.
 */
// FIXME: should we build all assembler nav at once, then go back and replace?!?
module.exports.register = function () {
  const assemblerNavs = new Map()

  this.once('contextStarted', () => {
    // NOTE register this extension after all extensions have been registered to ensure it's triggered last
    this.once('navigationBuilt', ({ contentCatalog }) => {
      for (const component of contentCatalog.getComponents()) {
        for (const componentVersion of component.versions) {
          if (!('_navigation' in componentVersion)) continue
          componentVersion.navigation = componentVersion._navigation
          delete componentVersion._navigation
        }
      }
    })
  })

  this.once('contentAggregated', ({ contentAggregate }) => {
    for (const componentVersionBucket of contentAggregate) {
      const { files, origins = [] } = componentVersionBucket
      const origin = origins.find((it) => it.descriptor?.ext?.assembler)
      if (!origin) continue
      const assemblerNav = origin.descriptor.ext.assembler.nav || []
      const assemblerNavFiles = assemblerNav.reduce((accum, it) => {
        const match = files.find((candidate) => candidate.path === it)
        if (match) accum.push(match)
        return accum
      }, [])
      // NOTE use descriptor here since we haven't yet established component version identity
      assemblerNavs.set(origin.descriptor, assemblerNavFiles)
    }
  })

  this.once('navigationBuilt', ({ contentCatalog, siteAsciiDocConfig }) => {
    const { buildNavigation } = this.getFunctions()
    for (const component of contentCatalog.getComponents()) {
      for (const componentVersion of component.versions) {
        const name = component.name
        const version = componentVersion.version
        let assemblerNavFiles
        for (const file of contentCatalog.findBy({ component: name, version })) {
          if ((assemblerNavFiles = assemblerNavs.get(file.src.origin.descriptor))) break
        }
        if (!assemblerNavFiles) continue
        assemblerNavFiles.forEach((assemblerNavFile, index) => {
          const segments = assemblerNavFile.path.split('/')
          let module_, relative
          if (segments[0] === 'modules') {
            module_ = segments[1]
            relative = segments.slice(2).join('/')
          } else {
            module_ = 'ROOT'
            relative = segments.join('/')
          }
          assemblerNavFile.src = { component: name, version, module: module_, family: 'nav', relative }
          assemblerNavFile.nav = { index }
        })
        const contentCatalogProxy = new Proxy(contentCatalog, {
          get (target, property) {
            if (property !== 'findBy') return target[property]
            return (criteria) => criteria.family === 'nav' ? assemblerNavFiles : target[property].apply(target, criteria)
          }
        })
        componentVersion._navigation = componentVersion.navigation
        const assemblerNavigation = buildNavigation(contentCatalogProxy, siteAsciiDocConfig).getNavigation(name, version)
        //componentVersion.navigation = componentVersion.navigationForAssembler = assemblerNavigation
        componentVersion.navigation = assemblerNavigation
      }
    }
  })
}
