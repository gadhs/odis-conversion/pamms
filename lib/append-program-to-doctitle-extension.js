'use strict'

const PROGRAM_NAMES = { medicaid: 'Medicaid' }

module.exports.register = function () {
  this.once('pagesComposed', ({ contentCatalog }) => {
    contentCatalog.getPages((page) => {
      const moduleName = page.src.module
      if (!page.out || moduleName === 'ROOT') return
      const programName = PROGRAM_NAMES[moduleName] || moduleName.toUpperCase()
      const pageTitle = `${page.title}<span class="is-hidden"> |\xa0${programName}</span>`
      const pageContents = page.contents
      const startTagIdx = pageContents.indexOf('<h1 class="page">')
      const endTagIdx = pageContents.indexOf('</h1>', startTagIdx)
      page.contents = Buffer.concat([
        pageContents.slice(0, startTagIdx),
        Buffer.from(`<h1 class="page">${pageTitle}`),
        pageContents.slice(endTagIdx),
      ])
    })
  })
}
