# alternate is to promote all nested entries to top level under the program
class ExtendedPDFConverter < (Asciidoctor::Converter.for 'pdf')
  register_for :pdf

  # only needed for dfcs when section_merge_strategy is discrete
  #def arrange_heading node, title, opts
  #  start_new_page if node.context == :section && node.level == 2
  #  super
  #end
end
